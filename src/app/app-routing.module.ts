import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollectionComponent } from './collection/collection.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NavComponent } from './nav/nav.component';
import { RacingBootsComponent } from './racing-boots/racing-boots.component';
import { ShoesComponent } from './shoes/shoes.component';

const routes: Routes = [

   { path: '', component:HomeComponent },
   { path: 'collection', component:CollectionComponent },
   { path: 'shoes', component:ShoesComponent },
   { path: 'racing-boots', component:RacingBootsComponent },
   { path: 'contact', component:ContactComponent },
   { path: 'nav', component:NavComponent },
   { path: 'footer', component:FooterComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
