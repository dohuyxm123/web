import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacingBootsComponent } from './racing-boots.component';

describe('RacingBootsComponent', () => {
  let component: RacingBootsComponent;
  let fixture: ComponentFixture<RacingBootsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacingBootsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacingBootsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
